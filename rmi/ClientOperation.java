import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import java.util.Scanner;

public class ClientOperation {

	private static RMIInterface lookUp;

	public static void main(String[] args)
		throws MalformedURLException, RemoteException, NotBoundException {

    Scanner sc = new Scanner(System.in);

		lookUp = (RMIInterface) Naming.lookup("rmi://localhost/MyServer");

    System.out.println("First number?");
    Integer x = sc.nextInt();

    System.out.println("Second number?");
    Integer y = sc.nextInt();

		Integer addRes = lookUp.add(x, y);
		Integer minusRes = lookUp.minus(x, y);
		Integer multRes = lookUp.mult(x, y);
		Integer divRes = lookUp.div(x, y);

    System.out.println("add result:");
    System.out.println(addRes);

    System.out.println("minus result:");
    System.out.println(minusRes);

    System.out.println("mult result:");
    System.out.println(multRes);

    System.out.println("div result:");
    System.out.println(divRes);
	}

}

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIInterface extends Remote {

    public String helloTo(String name) throws RemoteException;

    public Integer add(Integer x, Integer y) throws RemoteException;

    public Integer mult(Integer x, Integer y) throws RemoteException;

    public Integer minus(Integer x, Integer y) throws RemoteException;

    public Integer div(Integer x, Integer y) throws RemoteException;

}

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ServerOperation extends UnicastRemoteObject implements RMIInterface{

    protected ServerOperation() throws RemoteException {
        super();
    }

    @Override
    public String helloTo(String name) throws RemoteException{
        System.err.println(name + " is trying to contact!");
        return "Server says hello to " + name;
    }

    @Override
    public Integer add(Integer x, Integer y) throws RemoteException{
        return x + y;
    }

    @Override
    public Integer minus(Integer x, Integer y) throws RemoteException{
        return x - y;
    }

    @Override
    public Integer mult(Integer x, Integer y) throws RemoteException{
        return x * y;
    }

    @Override
    public Integer div(Integer x, Integer y) throws RemoteException{
        return x / y;
    }
    public static void main(String[] args){
        try {
            Naming.rebind("rmi://localhost/MyServer", new ServerOperation());
            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}

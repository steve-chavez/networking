# Networking exercises

## TCP and UDP

First compile java programs with:

```bash
javac TCPClient.java TCPServer.java
```

Then run the server:
```bash
java TCPServer
# Server listening on 8080
```

And then send a message from the client:
```bash
java TCPClient
## some message
## FROM SERVER: SOME MESSAGE
```

Run the UDP examples analogously.

## RMI

Compile all:

```bash
cd rmi
javac RMIInterface.java ClientOperation.java ServerOperation.java
```

You can use `rmic` to create a stub, though is deprecated:

```
rmic ServerOperation

Warning: generation and use of skeletons and static stubs for JRMP
is deprecated. Skeletons are unnecessary, and static stubs have
been superseded by dynamically generated stubs. Users are
encouraged to migrate away from using rmic to generate skeletons and static
stubs. See the documentation for java.rmi.server.UnicastRemoteObject.

ls | grep Stub
ServerOperation_Stub.class
```

Start the rmiregistry:

```
rmiregistry # a port can be passed
```

Start the server:

```
java ServerOperation

Server ready
```

Start the client:

```
java ClientOperation

What is your name?
John
Server says hello to John
```

## CORBA

Using the following openjdk version:

```
java --version
openjdk 10 2018-03-20
OpenJDK Runtime Environment Zulu10.1+11 (build 10+46)
OpenJDK 64-Bit Server VM Zulu10.1+11 (build 10+46, mixed mode)
```

Generate java classes from idl:

```
idlj -fall Hello.idl
```

Compile all(note the add-modules):

```
javac --add-modules java.corba *.java HelloApp/*.java
```

Start orbd:

```
orbd -ORBInitialPort 1050
```

Start server:

```
java --add-modules java.corba HelloServer -ORBInitialPort 1050 -ORBInitialHost localhost
## HelloServer ready and waiting ...
## HelloServer Exiting ...
```

Start client:

```
java --add-modules java.corba HelloClient -ORBInitialPort 1050 -ORBInitialHost localhost
## Obtained a handle on server object: IOR:000000000000001749444c3a48656c6c6f4170702f48656c6c6f3a312e300000000000010000000000000082000102000000000a3132372e302e312e3100a27100000031afabcb00000000204aacd7ea00000001000000000000000100000008526f6f74504f410000000008000000010000000014000000000000020000000100000020000000000001000100000002050100010001002000010109000000010001010000000026000000020002
## Hello world !!
```

## Resources

* TCP UDP
  + C implementations from
    - From https://ops.tips/blog/a-tcp-server-in-c/
    - https://www.geeksforgeeks.org/udp-server-client-implementation-c/
    - https://gist.github.com/miekg/a61d55a8ec6560ad6c4a2747b21e6128

  + Java implementations from
    - Chapter 2 Computer networking Top Down Approach - Kurose

* RMI 
  - https://en.wikipedia.org/wiki/Java_remote_method_invocation
  - https://www.mkyong.com/java/java-rmi-hello-world-example/
  - https://www.geeksforgeeks.org/remote-method-invocation-in-java/
	- https://docs.oracle.com/javase/7/docs/technotes/guides/rmi/hello/hello-world.html
  - https://sites.cs.ucsb.edu/~cappello/lectures/rmi/helloworld.shtml
	- Classes
    - https://docs.oracle.com/javase/8/docs/api/java/rmi/Remote.html
    - https://docs.oracle.com/javase/8/docs/api/java/rmi/server/UnicastRemoteObject.html
		- https://docs.oracle.com/javase/7/docs/api/java/rmi/Naming.html

* CORBA
  - https://docs.oracle.com/javase/7/docs/technotes/guides/idl/jidlExample.html
  - https://bugs.openjdk.java.net/browse/JDK-8190538

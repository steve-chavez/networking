# Fail on writing to a closed socket
# Program recieves a SIGPIPE so it finishes early
make failed_write

strace ./failed_write 2>&1 | tail -n 4 

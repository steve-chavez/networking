#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 8080
#define BACKLOG 4

typedef struct server {
	int listen_fd; // file descriptor of the socket in passive mode to wait for connections.
} server_t;

int server_listen(server_t* server){
  int err = 0;
	err = (server->listen_fd = socket(AF_INET, SOCK_STREAM, 0));
	if (err == -1) {
		perror("socket");
		printf("Failed to create socket endpoint\n");
		return err;
	}

	/* setsockopt: Handy debugging trick that lets us rerun the server immediately after we kill it; otherwise we have to wait about 20 secs.
	 * Eliminates "ERROR on binding: Address already in use" error.
	 */
	int optval = 1;
	setsockopt(server->listen_fd, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval, sizeof(int));

	// `sockaddr_in` provides ways of representing a full address
	// composed of an IP address and a port.
	//
	// sin_family   address family   AF_INET refers to the address family related to internet addresses
	// s_addr       address (ip) in network byte order (big endian)
	// sin_port     port in network byte order (big endian)
	struct sockaddr_in server_addr = { 0 };
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY); // INADDR_ANY is a special constant that signalizes "ANY IFACE", i.e., 0.0.0.0.
	server_addr.sin_port = htons(PORT);

	// bind() assigns the address specified to the socket referred
  // to by the file descriptor (`listen_fd`).
  //
  // Here we cast `sockaddr_in` to `sockaddr` and specify the
  // length such that `bind` can pick the values from the
  // right offsets when interpreting the structure pointed to.
  //
  // In this state the socket still won't show in netstat or ss
  // This is because the socket is in an active state
	err = bind(server->listen_fd,
	           (struct sockaddr*)&server_addr,
	           sizeof(server_addr));

	if (err == -1) {
		perror("bind");
		printf("Failed to bind socket to address\n");
		return err;
	}

	// listen() marks the socket referred to by sockfd as a passive socket,
	// that is, as a socket that will be used to accept incoming connection
	// requests using accept(2).
	err = listen(server->listen_fd, BACKLOG);
	if (err == -1) {
		perror("listen");
		printf("Failed to put socket in passive mode\n");
		return err;
	}
}

int server_accept(server_t* server)
{
	int err = 0;
	int conn_fd;
	socklen_t client_len;
	struct sockaddr_in client_addr;

	client_len = sizeof(client_addr);

	err = (conn_fd = accept(server->listen_fd, (struct sockaddr*)&client_addr, &client_len));
	if (err == -1) {
		perror("accept");
		printf("failed accepting connection\n");
		return err;
	}

  printf("client connected with ip address: %s\n", inet_ntoa(client_addr.sin_addr));

  int n = 0;
  int len = 0, maxlen = 100;
  char buffer[maxlen];
  char *pbuffer = buffer;

  // keep running as long as the client keeps the connection open
  while ((n = recv(conn_fd, pbuffer, maxlen, 0)) > 0) {
    pbuffer += n;
    maxlen -= n;
    len += n;

    printf("received: '%s'\n", buffer);

    // echo received content back
    send(conn_fd, buffer, len, 0);
  }

	err = close(conn_fd);
	if (err == -1) {
		perror("close");
		printf("failed to close connection\n");
		return err;
	}

	return err;
}

int main() {
	int err = 0;

	server_t server = { 0 };

	err = server_listen(&server);
	if (err) {
		printf("Failed to listen on address 0.0.0.0:%d\n", PORT);
		return err;
	}

	while(1) {
		err = server_accept(&server);
		if (err) {
			printf("Failed accepting connection\n");
			return err;
		}
	}

	return 0;
}

## We can see the file descriptors here
## Including the fd for the socket

make opening_socket

./opening_socket &

SERVER_PROC=$(pgrep opening_socket)

sudo lsof | ag $SERVER_PROC | ag sock

ls -lah /proc/$SERVER_PROC/fd

kill $SERVER_PROC

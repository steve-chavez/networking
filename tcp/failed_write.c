#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>

#define PORT 8080

typedef struct server {
	int listen_fd; // file descriptor of the socket in passive mode to wait for connections.
} server_t;

int server_listen(server_t* server){
  int err = 0;
	err = (server->listen_fd = socket(AF_INET, SOCK_STREAM, 0));
	if (err == -1) {
		perror("socket");
		printf("Failed to create socket endpoint\n");
		return err;
	}
}

int main() {
	int err = 0;
	server_t server = { 0 };

	err = server_listen(&server);

  err = write(server.listen_fd, "hey", 3);

  if (err == -1) {
    perror("write");
    printf("failed to write\n");
    return err;
  }

  for (;;) { }

	return 0;
}

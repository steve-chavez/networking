make bind_listen_socket

./bind_listen_socket &

# Check that `lsof` identifies our socket listening on port 8080
lsof -P -i:8080

# Check that `ss` properly recognizes our socket as `listen`
ss --listening --tcp --numeric | grep 8080

SERVER_PROC=$(pgrep bind_listen_soc)

# Get the inode of our socket.
#
# Given that `socket(2)` creates a file descriptor
# that is the next fd in the sequence (0, 1, and 2
# are already taken), we know that `3` references
# our socket.
SERVER_SOCKET=$(stat --dereference --printf %i /proc/$SERVER_PROC/fd/3)

# Check the state of our socket inode in the
# /proc/net/tcp table:
cat /proc/net/tcp | grep $SERVER_SOCKET

kill $SERVER_PROC

import java.io.*;
import java.net.*;
import java.nio.file.*;

class HTTPServer {
  static final int PORT = 8080;

  public static void main(String[] args) throws Exception, IOException {
    String clientSentence;
    ServerSocket welcomeSocket = new ServerSocket(PORT);
    System.out.println("Server listening on " + PORT);
    String root = new File("").getAbsolutePath() + File.separator;
    while(true){
      Socket connectionSocket = welcomeSocket.accept();
      new Thread(){
        public void run() {
          try{
            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
            String line;
            while ((line = inFromClient.readLine()) != null) {
              if (line.length() <= 0) {
                break;
              }
              if (line.startsWith("GET")) {
                String filename= line.split(" ")[1].substring(1);
                File resource = new File(root + filename);
                System.out.println("File: " + resource);
                if (resource.isFile()) {
                  String header = "HTTP/1.0 200 OK\n\n";
                  String body = new String(Files.readAllBytes(Paths.get(resource.toString())));
                  String res = header + body;
                  outToClient.write(res.getBytes());
                } else {
                  String header  = "HTTP/1.1 404 Not Found\n\n";
                  String body = "<html><head></head><body><h1>File Not Found</h1></body></html>";
                  String res = header + body;
                  outToClient.write(res.getBytes());
                }
                break;
              }
            }
            connectionSocket.close();
          } catch(IOException e){
            System.out.println(e);
          }
        }
      }.start();
    }
  }
}

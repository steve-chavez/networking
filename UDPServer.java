import java.io.*;
import java.net.*;

class UDPServer {
  static final int PORT = 8080;

  public static void main(String[] args) throws Exception{
    DatagramSocket serverSocket = new DatagramSocket(PORT);
    byte[] sendData = new byte[1024];
    byte[] receiveData = new byte[1024];
    System.out.println("Server listening on " + PORT);
    while(true){
      DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
      serverSocket.receive(receivePacket);
      String sentence = new String(receivePacket.getData());
      InetAddress ipAddress = receivePacket.getAddress();
      int clientPort = receivePacket.getPort();
      String capitalizedSentence = sentence.toUpperCase();
      sendData = capitalizedSentence.getBytes();
      DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ipAddress, clientPort);
      serverSocket.send(sendPacket);
    }
  }
}
